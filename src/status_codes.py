"""
- 1xx: Informational - Request received, continuing process

- 2xx: Success - The action was successfully received,
understood, and accepted

- 3xx: Redirection - Further action must be taken in order to
complete the request

- 4xx: Client Error - The request contains bad syntax or cannot
be fulfilled

- 5xx: Server Error - The server failed to fulfill an apparently
valid request
"""

codes = {
    100: "Continue",
    101: "Switching Protocols",
    
    200: "OK", # done
    201: "Created", # done
    202: "Accepted",
    203: "Non-Authoritative Information",
    204: "No Content", # done
    205: "Reset Content",
    206: "Partial Content", # done
    
    300: "Multiple Choices",
    301: "Moved Permanently",
    302: "Found",
    303: "See Other",
    304: "Not Modified", # done
    305: "Use Proxy",
    307: "Temporary Redirect",
    
    400: "Bad Request", # done
    401: "Unauthorized",
    402: "Payment Required",
    403: "Forbidden", # done
    404: "Not Found", # done
    405: "Method Not Allowed", # done
    406: "Not Acceptable", # done
    407: "Proxy Authentication Required",
    408: "Request Time-out", # done
    409: "Conflict",
    410: "Gone",
    411: "Length Required", # done
    412: "Precondition Failed", # done
    413: "Request Entity Too Large", # done
    414: "Request-URI Too Large", # done
    415: "Unsupported Media Type", # done
    416: "Requested range not satisfiable", # done
    417: "Expectation Failed",
    
    500: "Internal Server Error",
    501: "Not Implemented", # done
    502: "Bad Gateway",
    503: "Service Unavailable", # done
    504: "Gateway Time-out",
    505: "HTTP Version not supported" # done
}
