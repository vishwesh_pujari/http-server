import os

IP = "127.0.0.1"
Port = 8080

DocumentRoot = os.path.abspath(os.path.dirname(os.path.realpath(__file__)) + "/../data")

# for enabling persistent connections
KeepAlive = "True"

# maximum simultaneous persistent connections
MaxKeepAliveRequests = 10

# timeout for waiting for request
KeepAliveTimeout = 15

# methods implemented by server
Methods = ["GET", "HEAD", "PUT", "POST", "DELETE"]

Name = "http-server"
Version = 1.0

AccessLog = os.path.abspath(os.path.dirname(os.path.realpath(__file__)) + "/../access.log")
ErrorLog = os.path.abspath(os.path.dirname(os.path.realpath(__file__)) + "/../error.log")

# maximum permissible size of HTTP Request header
MaxHeaderSize = 8000

# Content-Encodings implemented
Encodings = ["gzip", "deflate", "identity"]

TransferEncodings = ["trailers"]

PIDFile = os.path.abspath(os.path.dirname(os.path.realpath(__file__)) + "/../pid")
CookieFile = os.path.abspath(os.path.dirname(os.path.realpath(__file__)) + "/../cookie.json")

LogLevel = "warn"
