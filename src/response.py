import status_codes

class ResponseGenerate:
    def __init__(self, statusCode: int = 200, headers: dict = dict(), body: bytes = None) -> None:
        self.statusLine = {
            "protocol": "HTTP",
            "majorVersion": 1,
            "minorVersion": 1,
            "statusCode": statusCode
        }
        self.headers = headers
        self.body = body
        return
    
    def genResponse(self) -> bytes:
        """
        Response      = Status-Line
                       *(( general-header
                        | response-header
                        | entity-header ) CRLF)
                       CRLF
                       [ message-body ]
        """
        
        statusLine = self.statusLine["protocol"] + "/" + str(self.statusLine["majorVersion"]) + "." + str(self.statusLine["minorVersion"]) + " " + str(self.statusLine["statusCode"]) + " " + status_codes.codes[self.statusLine["statusCode"]]

        headers = ""
        for header in self.headers:
            headers = headers + header + ": " + self.headers[header] + "\r\n"
        
        if self.body == None: 
            response = (statusLine + "\r\n" + headers + "\r\n").encode()
        else:
            response = (statusLine + "\r\n" + headers + "\r\n").encode() + self.body
        return response