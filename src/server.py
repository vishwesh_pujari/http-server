#!/usr/bin/python3

# standard libraries
import socket
import threading
import json # for pretty printing nested dict
import os

# user-defined libraries
import config
from request import RequestParse
from response import ResponseGenerate
from headers import Headers
from methods import Methods

class HttpServer:

    # TODO create .pyi file (see file of len from VS code) to create skeleton of module
    
    def __init__(self) -> None:
        # '-> None' indicates that this function returns None

        self.IP = config.IP
        self.port = config.Port
        self.name = config.Name
        self.version = config.Version
        self.timeout = config.KeepAliveTimeout
        self.implementedMethods = config.Methods # The method is case-sensitive.
        self.maxHeaderSize = config.MaxHeaderSize
        self.documentRoot = config.DocumentRoot
        if not os.path.exists(self.documentRoot):
            os.mkdir(self.documentRoot)
            # TODO also include your index.html
        self.maxConnections = config.MaxKeepAliveRequests
        self.keepAlive = config.KeepAlive
        self.currConnections = 0
        self.encodings = config.Encodings
        self.cookieFile = config.CookieFile
        self.pidFile = config.PIDFile
        if not os.path.exists(self.cookieFile):
            f = open(self.cookieFile, "w")
            f.write("{}")
            f.close()
        if not os.path.exists(self.pidFile):
            f = open(self.pidFile, "w")
            f.close()

        self.accessFile = config.AccessLog
        self.errorFile = config.ErrorLog
        self.methods = Methods(self.name, self.version, self.documentRoot, self.encodings)
        self.header = Headers(self.name, self.version, self.encodings)
        return

    def _handleRequest(self, connSock: socket.socket) -> tuple: # takes 1 request from a socket, returns a packet to be returned

        try:
            data = connSock.recv(self.maxHeaderSize) # https://stackoverflow.com/questions/686217/maximum-on-http-header-values
            if data == b"": # recv() returns b"" when the client has closed the connection!
                return None, None, None, None
        except socket.timeout:
            parsedRequest = RequestParse(b"", "", -1, [], self.maxHeaderSize)
            parsedRequest.invalid = True; parsedRequest.invalidCode = 408; parsedRequest.invalidReason = "Server timeout waiting for the HTTP request from the client."
            code, headers, sendPkt = self.methods.handleInvalidRequest(parsedRequest)
        except Exception as e:
            return None, None, None, None
        else:
            #print(data)
            parsedRequest = RequestParse(data, self.IP, self.port, self.implementedMethods, self.maxHeaderSize)
            if parsedRequest.invalid:
                code, headers, sendPkt = self.methods.handleInvalidRequest(parsedRequest)
                #return errPkt, None, parsedRequest
            else:
                if parsedRequest.requestLine["method"] == "GET":
                    code, headers, sendPkt = self.methods.GET(parsedRequest)
                    #ResponseGenerate(200, sendHeaders, data).genResponse()
                elif parsedRequest.requestLine["method"] == "HEAD":
                    code, headers, sendPkt = self.methods.HEAD(parsedRequest)
                elif parsedRequest.requestLine["method"] == "PUT":
                    code, headers, sendPkt = self.methods.PUT(parsedRequest, connSock)
                elif parsedRequest.requestLine["method"] == "POST":
                    code, headers, sendPkt = self.methods.POST(parsedRequest, connSock)
                    #print("POST request processing done")
                elif parsedRequest.requestLine["method"] == "DELETE":
                    code, headers, sendPkt = self.methods.DELETE(parsedRequest)

                if parsedRequest.invalid:
                    code, headers, sendPkt = self.methods.handleInvalidRequest(parsedRequest)
                    #return errPkt, None, parsedRequest

        
        if self.keepAlive == "False": # if KeepAlive is False in config file
            connection = "close"
        elif parsedRequest.headers.get("connection") != None:
            connection = parsedRequest.headers["connection"]
            if connection != "close" and connection != "keep-alive":
                connection = "close"
        else: # assuming the default value to be "close". This is however opposite to rfc specifications
            connection = "close"
        #return packet, connection, parsedRequest
        headers.update({
            "Connection": connection
        })

        if parsedRequest.headers != {}:
            userAgent = "-"
            if parsedRequest.headers.get("user-agent") != None:
                userAgent = parsedRequest.headers["user-agent"]

            f = open(self.cookieFile, "r")
            cookieData = json.load(f)
            if parsedRequest.headers.get("cookie") == None:
                newCookie = self.header.set_cookie()
                cookieData.update({
                    newCookie: [1, userAgent]
                })
                headers.update({
                    "Set-Cookie": "RandomCookie=" + newCookie
                })
            else:
                recvCookie = self.header.cookie(parsedRequest.headers["cookie"])
                # TODO assert recvCookie[0] == "RandomCookie" here
                if cookieData.get(recvCookie[0][1]) != None: # the cookie value is valid
                    cookieData[recvCookie[0][1]][0] += 1
            prettyStr = json.dumps(cookieData, indent=4)
            f.close()
            f = open(self.cookieFile, "w")
            f.write(prettyStr)
            f.close()
        parsedResponse = ResponseGenerate(code, headers, sendPkt)
        packet = parsedResponse.genResponse()
        return parsedRequest, parsedResponse, packet, connection

    def _multiThread(self, connSock: socket.socket, addr: tuple) -> None:
        f = open(self.accessFile, "a")
        f.write(addr[0] + ":" + str(addr[1]) + " started on a new thread")
        f.write("\n")
        f.close()
        self.currConnections += 1
        while True:
            parsedRequest, parsedResponse, packet, connection = self._handleRequest(connSock)
            if (parsedRequest, parsedResponse, packet, connection) == (None, None, None, None): # it means that the client has closed the connection
                break
            
            if parsedRequest.invalidCode != 408: # if timeout hasn't happened
                self._accessLog(parsedRequest, parsedResponse, addr)
            if parsedRequest.invalid:
                self._errorLog(parsedRequest, addr)
            connSock.sendall(packet)

            if connection == "close":
                connSock.close()
                break
            elif connection == "keep-alive":
                continue
        f = open(self.accessFile, "a")
        f.write(addr[0] + ":" + str(addr[1]) + " thread ended")
        f.write("\n")
        f.close()
        self.currConnections -= 1
        return

    def _accessLog(self, parsedRequest: RequestParse, parsedResponse: ResponseGenerate, addr: tuple) -> None:
        f = open(self.accessFile, "a")
        f.write(addr[0] + ":" + str(addr[1]) + " " + "[" + self.header.date() + "]" + " ")
        try:
            f.write('"' + parsedRequest.requestLine["method"] + " " + parsedRequest.requestLine["URI"]["path"] + " " + parsedRequest.requestLine["version"]["protocol"] + "/" + str(parsedRequest.requestLine["version"]["majorVersion"]) + "." + str(parsedRequest.requestLine["version"]["minorVersion"]) + '" ' + str(parsedResponse.statusLine["statusCode"]) + " ")
        except:
            f.write(str(parsedResponse.statusLine["statusCode"]) + " ")           
        
        if parsedResponse.body == None:
            f.write("0")
        else:
            f.write(str(len(parsedResponse.body)) + " ")
        if parsedRequest.headers.get("user-agent") != None:
            f.write('"' + parsedRequest.headers["user-agent"] + '"')
        else:
            f.write('"-"')
        f.write("\n")
        f.close()
        return

    def _errorLog(self, parsedRequest: RequestParse, addr: tuple) -> None:
        if not parsedRequest.invalid:
            return
        f = open(self.errorFile, "a")

        try:
            f.write(addr[0] + ":" + str(addr[1]) + " " + "[" + self.header.date() + "]" + " " + '"' + parsedRequest.requestLine["method"] + " " + parsedRequest.requestLine["URI"]["path"] + " " + parsedRequest.requestLine["version"]["protocol"] + "/" + str(parsedRequest.requestLine["version"]["majorVersion"]) + "." + str(parsedRequest.requestLine["version"]["minorVersion"]) + '" ' + str(parsedRequest.invalidCode) + " " + '"' + parsedRequest.invalidReason + '"')
        except:
            f.write(addr[0] + ":" + str(addr[1]) + " " + "[" + self.header.date() + "]" + " " + str(parsedRequest.invalidCode) + " " + '"' + parsedRequest.invalidReason + '"')
        f.write("\n")
        f.close()
        return

    def start(self) -> None:
        # Create socket
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except Exception as e:
            print("socket.socket() failed: ", e)
            return

        # Bind socket
        try:
            s.bind((self.IP, self.port))
            s.listen(1)
        except Exception as e:
            s.close()
            print("bind() failed: ", e)
            return
        
        #print("Listening at", s.getsockname())

        while True:
            try:
                connSock, addr = s.accept()
                connSock.settimeout(self.timeout)
            except:
                s.close()
                break
            #print(addr, "Connected")

            parsedRequest, parsedResponse, packet, connection = self._handleRequest(connSock)
            if (parsedRequest, parsedResponse, packet, connection) == (None, None, None, None): # this means that client has closed the connection
                continue
            if parsedRequest.invalidCode != 408: # if timeout error hasn't happened
                self._accessLog(parsedRequest, parsedResponse, addr)
            
            if connection == "close":
                connSock.sendall(packet)
                connSock.close()
            elif connection == "keep-alive":
                if self.currConnections >= self.maxConnections:
                    parsedRequest = RequestParse(b"", "", -1, [], self.maxHeaderSize)
                    parsedRequest.invalid = True; parsedRequest.invalidCode = 503; parsedRequest.invalidReason = "Maximum number of requests for persistent connections(" + str(self.maxConnections) + ")exceeded"
                    code, headers, sendPkt = self.methods.handleInvalidRequest(parsedRequest)
                    errPkt = ResponseGenerate(code, headers, sendPkt).genResponse()
                    connSock.sendall(errPkt)
                    connSock.close()
                else:
                    connSock.sendall(packet)
                    threading.Thread(target=self._multiThread, args=(connSock,addr)).start()
            
            if parsedRequest.invalid:
                self._errorLog(parsedRequest, addr)
        return
