#!/usr/bin/bash

chmod -r ../data/hello
python3 main.py &

# $! is the PID of the last background process
echo $! > ../pid
