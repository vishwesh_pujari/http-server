import os
from socket import socket # to check whether file exists or not
import stat
import mimetypes
import subprocess # to execute 'file' shell command
import gzip # gzip
import zlib # deflate
import datetime
import re
from urllib.parse import unquote

from request import RequestParse
from response import ResponseGenerate
from headers import Headers
import status_codes

class Methods:
    def __init__(self, name: str, version: float, documentRoot: str, encodings: list) -> None:
        self.header = Headers(name, version, encodings)
        self.documentRoot = documentRoot
        self.encodings = encodings
        return

    def _HEAD(self, parsedRequest: RequestParse) -> tuple: # used in both GET and HEAD
        """
        406 is returned by the server when it can't respond based on accepting the request headers (ie they have an Accept header which states they only want XML).
        415 is returned by the server when the entity sent in a request (content in a POST or PUT) has an unsupported mediatype (i.e. they sent XML).
        so.. 406 when you can't send what they want, 415 when they send what you don't want.
        """
        path = parsedRequest.requestLine["URI"]["path"]
        if path == "/": # if path is / consider it as index.html
            path = "/index.html"

        if not os.path.exists(self.documentRoot + path):
            parsedRequest.invalid = True
            parsedRequest.invalidCode = 404
            return None, None, None, path + " not found on this server"
        if os.path.isdir(self.documentRoot + path):
            """
            TODO - Send a table containing all files just like apache does
            """

        status = os.stat(self.documentRoot + path)
        modifiedDate = datetime.datetime.utcfromtimestamp(int(status.st_mtime)) # the int here is used to eliminate the microseconds as http dates don't contain microsecond precision
        
        permissions = stat.filemode(status.st_mode)
        otherPermissions = permissions[7:] # permissions for 'other' user: https://www.guru99.com/file-permissions.html
        if otherPermissions[0] != "r":
            parsedRequest.invalid = True
            parsedRequest.invalidCode = 403 # forbidden
            return None, None, None, "Read permission not there on " + path
        
        data = subprocess.Popen(["file", "--mime-type", self.documentRoot + path], stdout = subprocess.PIPE)
        output = data.communicate()
        actType = output[0].decode().strip().split(":")[-1].strip().split("/") # actual mime type
        # the field names in headers are case insensitive
        if parsedRequest.headers.get("accept") != None:

            reqTypes = self.header.accept(parsedRequest.headers["accept"]) # requested types
            
            i = 0
            while i < len(reqTypes):
                type = reqTypes[i][0]
                subtype = reqTypes[i][1]
                
                if type == "*" and subtype == "*":
                    break
                if subtype == "*" and type == actType[0]:
                    break
                if type == actType[0] and subtype == actType[1]:
                    break
                i += 1
            
            if i == len(reqTypes):
                # the type didn't match
                parsedRequest.invalid = True
                parsedRequest.invalidCode = 406
                return None, None, None, "Type of " + path + " is " + actType[0] + "/" + actType[1] + " which is not present in Accept Header"
            
        data = subprocess.Popen(["file", "--mime-encoding", self.documentRoot + path], stdout = subprocess.PIPE)
        output = data.communicate()
        actCharset = output[0].decode().strip().split(":")[1].strip() # actual charset
        if parsedRequest.headers.get("accept-charset") != None:
            # https://www.iana.org/assignments/character-sets/character-sets.xhtml
            reqCharset = self.header.accept_charset(parsedRequest.headers["accept-charset"]) # requested charset
            
            i = 0
            while i < len(reqCharset):
                if reqCharset[i] == "*":
                    break
                if reqCharset[i] == actCharset:
                    break
                i += 1
            if i == len(reqCharset):
                parsedRequest.invalid = True
                parsedRequest.invalidCode = 406
                return None, None, None, "Charset of " + path + " is " + actCharset + " which is not present in Accept-Charset"
        
        accept_encoding = ""
        if parsedRequest.headers.get("accept-encoding") != None:
            reqEnc = self.header.accept_encoding(parsedRequest.headers["accept-encoding"])
            acceptedEncodings = self.encodings
            i = 0
            while i < len(reqEnc):
                if reqEnc[i] in acceptedEncodings or reqEnc[i] == "*":
                    break
                i += 1
            if i == len(reqEnc):
                parsedRequest.invalid = True
                parsedRequest.invalidCode = 406
                return None, None, None, "Accept-Encoding values unacceptable. Accepted values: gzip, deflate, identity" # TODO - do this dynamically!
            else:
                accept_encoding = reqEnc[i]

        if parsedRequest.headers.get("if-modified-since") != None:
            if self.header.if_modified_since(modifiedDate, parsedRequest.headers["if-modified-since"]) == False:
                parsedRequest.invalid = True
                parsedRequest.invalidCode = 304
                return None, None, None, "" # no message body to be sent            

        if parsedRequest.headers.get("if-unmodified-since") != None:
            if self.header.if_unmodified_since(modifiedDate, parsedRequest.headers["if-unmodified-since"]) == False:
                parsedRequest.invalid = True
                parsedRequest.invalidCode = 412 # precondition failed
                parsedRequest.sendHeaders = {
                    "Last-Modified": self.header.last_modified(modifiedDate)
                }
                return None, None, None, "" # no message body to be sent

        te = ""
        if parsedRequest.headers.get("te") != None:
            if parsedRequest.headers["te"].strip() != "trailers": # TODO add this into headers.py
                parsedRequest.invalid = True
                parsedRequest.invalidCode = 501
                parsedRequest.sendHeaders = {
                    "Allow": "trailers" # TODO do this dynamically
                }
                return None, None, None, parsedRequest.headers["te"].strip() + " not implemented. Accepted Values: trailers"
            te = "chunked"

        sendHeaders = {
            "Date": self.header.date(),
            "Server": self.header.server(),
            "Last-Modified": self.header.last_modified(modifiedDate),
            "Content-Type": actType[0] + "/" + actType[1],
            "Accept-Ranges": "bytes"
        }
        
        if actType[0] == "text":
            mode = "rt"
        else:
            mode = "rb"
        f = open(self.documentRoot + path, mode)

        # if te has been given but accept_encoding hasn't been given
        if te != "" and accept_encoding == "":
            data = b""
            chunk = 1024 # one chunk will be of this size TODO - how to change this size?
            while True:
                d = f.read(chunk)
                if len(d) == 0:
                    break
                if mode == "rt":
                    d = d.encode()
                # the length here is in terms of hex string!
                data += (hex(len(d))[2:] + "\r\n").encode() + d + "\r\n".encode()
            data += b"0\r\n\r\n"
            sendHeaders.update({
                "Transfer-Encoding": "chunked"
            })
        # otherwise if both te and accept_encoding are given
        # then accept_encoding will be given higher preference
        else:
            data = f.read()
            if mode == "rt": 
                data = data.encode()

            if actType[0] != "image": # image files are already compressed.
                if accept_encoding == "gzip":
                    data = gzip.compress(data)
                    sendHeaders.update({
                        "Content-Encoding": "gzip"
                    })
                elif accept_encoding == "deflate":
                    data = zlib.compress(data)
                    sendHeaders.update({
                        "Content-Encoding": "deflate"
                    })
            sendHeaders.update({
                "Content-Length": str(len(data))
            })

        rangePresent = False
        if parsedRequest.headers.get("range") != None:
            rangePresent = True
            try:
                Range = self.header.range(parsedRequest.headers["range"], len(data))
            except Exception as e:
                
                parsedRequest.invalid = True
                parsedRequest.invalidCode = 416
                return None, None, None, str(e)

        # if both If-Range and Range are present
        if parsedRequest.headers.get("if-range") != None and rangePresent:
            ifh = parsedRequest.headers["if-range"] # if range header

            # because if entity has been changed then rangePresent must be False
            # because we want to send entire doc
            rangePresent = not self.header.if_range(ifh, self.documentRoot + path, modifiedDate)

        # if te not given
        code = 200
        if te == "" and rangePresent:
            code = 206
            if len(Range) == 1:
                sendHeaders.update({
                    "Content-Length": str(Range[0][1] - Range[0][0] + 1),
                    "Content-Range": "bytes " + str(Range[0][0]) + "-" + str(Range[0][1]) + "/" + str(len(data))
                })
                data = data[Range[0][0]: Range[0][1] + 1]
            else:
                multipartData = b""
                boundary = "49eead1f4b0e76de"
                for r in Range:
                    multipartData += b"\r\n--" + boundary.encode() + b"\r\n" + ("Content-Type: " + actType[0] + "/" + actType[1] + "\r\n" + "Content-Range: bytes " + str(r[0]) + "-" + str(r[1]) + "/" + str(len(data)) + "\r\n\r\n").encode() + data[r[0]: r[1] + 1]
                multipartData += b"\r\n--" + boundary.encode() + b"--\r\n"

                sendHeaders["Content-Type"] = "multipart/byteranges; boundary=" + boundary
                sendHeaders.update({
                    "Content-Length": str(len(multipartData)),
                })
                data = multipartData

        # Content-MD5 header is applied only if Transfer-Encoding hasn't been applied
        if te == "":
            sendHeaders.update({
                "Content-MD5": self.header.content_md5(data)
            })

        # TODO append -gzip for gzip encoding etc.
        sendHeaders.update({
            "ETag": '"' + self.header.get_ETag(self.documentRoot + path) + '"'
        })
        f.close()
        # send code also from here
        return code, sendHeaders, data, ""
        
    def GET(self, parsedRequest: RequestParse) -> bytes:
        code, sendHeaders, data, invalidString = self._HEAD(parsedRequest)
        if parsedRequest.invalid == True:
            parsedRequest.invalidReason = invalidString
            return b"", None, None

        return code, sendHeaders, data
    
    def HEAD(self, parsedRequest: RequestParse) -> bytes:
        code, sendHeaders, data, invalidString = self._HEAD(parsedRequest)
        if parsedRequest.invalid == True:
            parsedRequest.invalidReason = "" # we don't send any entity from HEAD
            return b"", None, None

        return code, sendHeaders, None

    def _createPath(self, documentRoot: str, path: str) -> str: # creates directories if not already present
        if os.path.isdir(documentRoot + path): # if given path itself is a directory then return the path only!
            if path[-1] == "/":
                path = path[:-1]
            return path
        directories = path.split("/")[1:-1] # first is "" and last is name of file
        currDir = os.getcwd()
        os.chdir(documentRoot)
        dirPath = ""
        for dir in directories:
            try:
                os.mkdir(dir)
            except:
                pass
            finally:
                os.chdir(dir)
                dirPath += "/" + dir
        os.chdir(currDir)
        return dirPath

    def _PUTPOST(self, parsedRequest: RequestParse, connSock: socket) -> tuple: # common code betn POST and PUT
        lengthReceived = len(parsedRequest.body)
        if parsedRequest.headers.get("content-length") != None:
            try:
                actualLength = int(parsedRequest.headers["content-length"])
            except:
                parsedRequest.invalid = True
                parsedRequest.invalidReason = "Invalid Content-Length header"
                parsedRequest.invalidCode = 400
                return None, None
        else:
            parsedRequest.invalid = True
            parsedRequest.invalidCode = 411
            parsedRequest.invalidReason = "Content-Length header not present."
            return None, None

        remainingData = b""
        data = parsedRequest.body
        while actualLength > lengthReceived: # get the remaining data (if present) from the socket
            remainingData = connSock.recv(actualLength - lengthReceived)
            lengthReceived += len(remainingData)
            data = data + remainingData
        
        # TODO - complete the Transfer Encoding, Content-MD5 here
        if parsedRequest.headers.get("content-encoding") != None:
            encoding = self.header.content_encoding(parsedRequest.headers["content-encoding"])
            if encoding[0] == False:
                parsedRequest.invalid = True
                parsedRequest.invalidReason = encoding[1] + " not supported"
                parsedRequest.invalidCode = 415
                return None, None
            try:
                #print("I am here")
                if encoding[1] == "gzip":
                    data = gzip.decompress(data)
                elif encoding[1] == "deflate":
                    data = zlib.decompress(data)
            except:
                parsedRequest.invalid = True
                parsedRequest.invalidCode = 400
                parsedRequest.invalidReason = "Invalid Content-Encoding " + encoding[1]
                return None, None
        # we can be assured that data is appropriately decompressed
        mode = "wb"
        if parsedRequest.headers.get("content-type") != None:
            typeCharset = self.header.content_type(parsedRequest.headers["content-type"])
            if typeCharset[0] == "text" or (typeCharset[0] == "application" and typeCharset[1] == "x-www-form-urlencoded"):
                mode = "wt"
        
        if mode == "wt":

            try:
                if typeCharset[2] == '':
                    data = data.decode()
                else:
                    try:
                        data = data.decode(encoding=typeCharset[2])
                    except:
                        parsedRequest.invalid = True
                        parsedRequest.invalidReason = "Invalid charset value in Content-Type header"
                        parsedRequest.invalidCode = 400
                        return None, None
            except:
                parsedRequest.invalid = True
                parsedRequest.invalidReason = "Invalid Content-Type header"
                parsedRequest.invalidCode = 400
                return None, None
        
        return mode, data
    
    def PUT(self, parsedRequest: RequestParse, connSock: socket) -> bytes:
        """The recipient of the entity MUST NOT ignore any Content-*
        (e.g. Content-Range) headers that it does not understand or implement
        and MUST return a 501 (Not Implemented) response in such cases."""

        if parsedRequest.requestLine["method"] != "PUT":
            return b"", None, None
        path = parsedRequest.requestLine["URI"]["path"]
        if os.path.isdir(self.documentRoot + path):
            parsedRequest.invalid = True
            parsedRequest.invalidCode = 405
            parsedRequest.invalidReason = path + " is a directory. PUT request not allowed on a directory"
            parsedRequest.sendHeaders = {
                "Allow": "GET, HEAD, POST, DELETE"
            }
            return b"", None, None
        
        # we can safely assume here that path provided is not a directory
        # Also the path will always begin with '/' as we have already validated it
        self._createPath(self.documentRoot, path)

        # we can safely assume that the path provided is created
        mode, data = self._PUTPOST(parsedRequest, connSock)
        if (mode, data) == (None, None):
            return b"", None, None
        
        if os.path.exists(self.documentRoot + path):
            code = 204 # No Content
        else:
            code = 201 # Created
        #print(mode)
        f = open(self.documentRoot + path, mode)
        f.write(data)
        f.close()

        sendHeaders = { # we can send additional info like Last-Modified or ETag also from PUT
            "Date": self.header.date(),
            "Server": self.header.server(),
        }

        # For PUT request we will send a Location Header and not Content-Location header because
        """
        The Content-Location entity-header field MAY be used to supply the
        resource location for the entity enclosed in the message when that
        entity is accessible from a location SEPARATE from the requested
        resource's URI.
        """
        if code == 201:
            sendPkt = self._errorMessage(str(code) + " " + status_codes.codes[code], status_codes.codes[code], "New resource created at http://localhost:8080" + path)
            """For 201 (Created)
            responses, the Location is that of the new resource which was created
            by the request."""
            sendHeaders.update({
                # TODO - Please do the port and IP given below dynamically!!
                "Location": "http://localhost:8080" + path, # Location = "Location" ":" absoluteURI,
                "Content-Length": str(len(sendPkt))
            })
            
            return code, sendHeaders, sendPkt # TODO - do we need to end a Body from here?
        return code, sendHeaders, None

    def POST(self, parsedRequest: RequestParse, connSock: socket) -> bytes:
        # TODO - handle Range uploads to server: https://stackoverflow.com/questions/20969331/standard-method-for-http-partial-upload-resume-upload
        if parsedRequest.requestLine["method"] != "POST":
            return b"", None, None
        path = parsedRequest.requestLine["URI"]["path"]
        
        isDir = False
        filename = path.split("/")[-1]
        if os.path.isdir(self.documentRoot + path):
            isDir = True
            filename = ""

        dirPath = self._createPath(self.documentRoot, path)
        currDir = os.getcwd()
        os.chdir(self.documentRoot + dirPath)
        ls = os.listdir(".")
        
        postNum = float("-inf")
        for file in ls: # get the appropriate POST number
            valid = False
            if isDir == True and re.fullmatch("post[0-9]+", file) != None:
                valid = True
            if isDir == False and re.fullmatch("post[0-9]+[.]" + filename, file) != None:
                valid = True
            if valid:
                #print(file)
                isDigit = 4 # first 4 chars are post
                while isDigit < len(file) and 48 <= ord(file[isDigit]) <= 57:
                    isDigit += 1
                if int(file[4:isDigit]) > postNum:
                    postNum = int(file[4:isDigit])
        
        if postNum == float("-inf"):
            postNum = 0
        
        mode, data = self._PUTPOST(parsedRequest, connSock)
        if (mode, data) == (None, None):
            return b"", None, None

        postNum += 1
        if isDir == False:
            newfilename = "post" + str(postNum) + "." + filename
        else:
            newfilename = "post" + str(postNum)
        f = open(newfilename, mode)

        if parsedRequest.headers.get("content-type") != None:
            type = self.header.content_type(parsedRequest.headers["content-type"])

            # content-types for POST: https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST
            if type[0] == "application" and type[1] == "x-www-form-urlencoded":
                # https://developer.mozilla.org/en-US/docs/Glossary/percent-encoding
                # Depending on the context, the character ' ' is translated to a '+' (like in the percent-encoding version used in an application/x-www-form-urlencoded message), or in '%20' like on URLs.
                data = data.replace("+", " ") # the form-urlencoded encodes " " as "+"
                data = unquote(data) # IMPORTTANT! - Bcoz some chars are % encoded

                data = data.replace("&", "\r\n") # this replacement is done for the sake of pretty output in the file
                f.write(data)
            # https://datatracker.ietf.org/doc/html/rfc1867#section-6
            elif type[0] == "multipart" and type[1] == "form-data":
                boundary = type[2]
                splitData = data.split(b"\r\n")
                i = 0
                while b"--" + bytes(boundary, "utf-8") + b"--" != splitData[i]:
                    i += 1
                    try:
                        #print(repr(str(splitData[i].split(b":", 1)[0])[2:-1].lower().strip()))
                        assert str(splitData[i].split(b":", 1)[0])[2:-1].lower().strip() == "content-disposition"
                        cdh = str(splitData[i].split(b":", 1)[1])[2:-1].strip() # content-disposition header
                    except:
                        #print("1")
                        parsedRequest.invalid = True
                        parsedRequest.invalidCode = 400
                        parsedRequest.invalidReason = "Invalid Content-Disposition header"
                        return b"", None, None
                    ret = self.header.content_disposition(cdh)
                    if not ret[0]:
                        #print("2")
                        parsedRequest.invalidCode = 400
                        parsedRequest.invalid = True
                        parsedRequest.invalidReason = "Invalid Content-Disposition header"
                        return b"", None, None
                    if len(ret) == 2:
                        i += 1
                    else: # if len(ret) in content-disposition then Content-Type header also present
                        # TODO - extract the content-type and charset here
                        i += 2
                    try:
                        assert splitData[i] == b""
                    except:
                        #print("3")
                        parsedRequest.invalid = True
                        parsedRequest.invalidCode = 400
                        parsedRequest.invalidReason = "Empty line not present after Content-Disposition header"
                        return b"", None, None
                    i += 1
                    value = b""
                    while b"--" + bytes(boundary, "utf-8") != splitData[i] and b"--" + bytes(boundary, "utf-8") + b"--" != splitData[i]:
                        value += splitData[i] + b"\r\n"
                        #print(b"--" + bytes(boundary, "utf-8"))
                        i += 1
                    value = value[:-2] # remove last 2 \r\n
                    f.write(bytes(ret[1], "utf-8") + b"=")
                    if len(ret) == 2:
                        f.write(value + b"\r\n")
                    else:
                        f.write(b"./" + bytes(ret[2], "utf-8") + b"\r\n")
                        #print("creating valuefilename")
                        if ret[2] != "":
                            f1 = open(ret[2], "wb")
                            f1.write(value)
                            f1.close()
            else: # if some other content-type given
                f.write(data)
        else: # if content-type not given
            f.write(data)

        f.close()
        sendHeaders = { # we can send additional info like Last-Modified or ETag also from PUT
            "Date": self.header.date(),
            "Server": self.header.server(),
            "Content-Location": dirPath + "/" + newfilename
        }
        os.chdir(currDir)
        sendPkt = self._errorMessage(str(201) + " " + status_codes.codes[201], status_codes.codes[201], "New resource created at http://localhost:8080" + path)
        sendHeaders.update({
            "Content-Length": str(len(sendPkt))
        })
        
        return 201, sendHeaders, sendPkt # TODO - do we need to end a Body from here?
    
    def DELETE(self, parsedRequest: RequestParse) -> bytes:
        if parsedRequest.requestLine["method"] != "DELETE":
            return b"", None, None
        path = parsedRequest.requestLine["URI"]["path"]
        resource = self.documentRoot + path

        if not os.path.exists(resource):
            parsedRequest.invalidCode = 404
            parsedRequest.invalid = True
            parsedRequest.invalidReason = path + " not found"
            return b"", None, None

        if os.path.isdir(resource):
            if os.listdir(resource) != []:
                parsedRequest.invalid = True
                parsedRequest.invalidCode = 403
                parsedRequest.invalidReason = "Directory " + path + " not empty"
                return b"", None, None
            else:
                os.rmdir(resource)
        else:
            os.remove(resource)
        
        sendHeaders = { # we can send additional info like Last-Modified or ETag also from PUT
            "Date": self.header.date(),
            "Server": self.header.server()
        }
        return 204, sendHeaders, None
    
    def handleInvalidRequest(self, parsedRequest: RequestParse) -> bytes:
        """
        However, it is "good practice" to send
        general-header fields first, followed by request-header or response-
        header fields, and ending with the entity-header fields.
        """

        # if request is valid then don't send response from this function
        if parsedRequest.invalid == False:
            return b"", None, None

        code = parsedRequest.invalidCode

        headers = {
                "Date": self.header.date(),
                "Server": self.header.server()
        }
        sendPkt = None
        if parsedRequest.invalidReason != "":
            sendPkt = self._errorMessage(str(code) + " " + status_codes.codes[code], status_codes.codes[code], parsedRequest.invalidReason)
            headers.update({
                "Content-Length": str(len(sendPkt)),
                "Content-Type": "text/html"
            })
        
        headers.update(parsedRequest.sendHeaders)
        
        return code, headers, sendPkt
  
    def _errorMessage(self, title: str, h1: str, p: str) -> bytes: # TODO - use this errorMessage as a generic message!
        err = '<!DOCTYPE html><html lang="en"><head><title>' + title + '</title></head><body><h1>' + h1 + '</h1><p>' + p + '</p></body></html>'
        return err.encode()
