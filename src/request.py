from urllib.parse import unquote, urlparse # for parsing URIs

class RequestParse:
    def __init__(self, request: bytes, IP: str, port: int, methods: list, maxHeaderSize: int) -> None:
        self.requestLine = {
            "method": None,
            "URI": {
                "scheme": None,
                "host": None,
                "port": None,
                "path": None,
                "params": None,
                "query": None,
                "fragment": None
            },
            "version": {
                "protocol": None,
                "majorVersion": None,
                "minorVersion": None
            }
        }
        self.headers = {}
        self.body = None
        self.invalid = False
        self.invalidReason = None
        self.invalidCode = None # The invalid status code
        self.sendHeaders = {} # The headers to be sent when the request is invalid
        self.IP = IP
        self.port = port
        self.methods = methods
        self.maxHeaderSize = maxHeaderSize
        self._parseRequest(request)
        self._validateRequest()
        return
    
    def _parseRequestLine(self, requestLine: str) -> None:
        """
        parsing the requestLine part
        https://datatracker.ietf.org/doc/html/rfc2616#section-5.1
        Request-Line   = Method SP Request-URI SP HTTP-Version CRLF
        """


        if len(requestLine.split(" ")) != 3:
            self.invalid = True
            self.invalidReason = "Invalid Request Line"
            return

        [method, URI, httpVersion] = requestLine.split(" ")
        
        # Servers ought to be cautious about depending on URI lengths
        # above 255 bytes, because some older client or proxy
        # implementations might not properly support these lengths.
        if len(URI) > 255:
            self.invalid = True
            self.invalidReason = "Maximum length of URI(255) exceeded"
            self.invalidCode = 414 # https://stackoverflow.com/questions/686217/maximum-on-http-header-values
            return
        
        # parsing the method
        self.requestLine["method"] = method
        URI = unquote(URI) # Remove all the ASCII Encoding: https://www.w3schools.com/tags/ref_urlencode.ASP
        parseURIObj = urlparse(URI)
        self.requestLine["URI"]["scheme"] = parseURIObj.scheme.lower() # http
        self.requestLine["URI"]["host"] = parseURIObj.netloc.split(":", 1)[0].lower() # BEWARE THAT THIS HOST is not the header. So make nested dicts
        
        try:
            self.requestLine["URI"]["port"] = int(parseURIObj.netloc.split(":", 1)[1])
        except:
            self.requestLine["URI"]["port"] = self.port # default port is assumed to be 8080 instead of 80
        
        if parseURIObj.path == "":
            self.requestLine["URI"]["path"] = "/"
        else:
            self.requestLine["URI"]["path"] = parseURIObj.path
        if self.requestLine["URI"]["path"][0] != "/":
            self.invalid = True
            self.invalidReason = "Invalid Path. / missing in the beginning"
            return
        
        self.requestLine["URI"]["params"] = parseURIObj.params
        self.requestLine["URI"]["query"] = parseURIObj.query
        self.requestLine["URI"]["fragment"] = parseURIObj.fragment

        # parsing The httpVersion
        protocol = httpVersion.split("/", 1)[0]
        try:
            version = httpVersion.split("/", 1)[1]
        except: # if only protocol name is given and version number isn't given
            self.invalid = True
            self.invalidReason = "Version number not specified"
            return

        self.requestLine["version"]["protocol"] = protocol
        
        # HTTP uses a "<major>.<minor>" numbering scheme to indicate versions of the protocol
        try:
            self.requestLine["version"]["majorVersion"] = int(version.split(".", 1)[0])
            self.requestLine["version"]["minorVersion"] = int(version.split(".", 1)[1])
        except: # if any of the version numbers aren't integers
            self.invalid = True
            self.invalidReason = "Invalid Version number"
            return
        return

    def _parseHeaders(self, headers: list) -> None:
        for header in headers: # splitHeader[0] is requestLine
            # Field names are case-insensitive.: https://datatracker.ietf.org/doc/html/rfc2616#section-4.2
            fieldName = header.split(":", 1)[0].lower()

            try:
                fieldValue = header.split(":", 1)[1].strip()
            except: # ":" wasn't present
                self.invalid = True
                self.invalidReason = "Field value not present for " + fieldName + " header"
                return

            # Combining duplicate field-name as a "," separated list
            if self.headers.get(fieldName) == None:
                self.headers[fieldName] = fieldValue
            else:
                self.headers[fieldName] += "," + fieldValue
        return

    def _parseRequest(self, request: bytes) -> None:
        # Remove all initial CRLF
        request = request.lstrip(b"\r\n")

        # find the position of 2*CRLF
        i = request.find(b"\r\n\r\n")
        
        if i == -1 and len(request) >= self.maxHeaderSize:
            self.invalid = True
            self.invalidReason = "Maximum size of headers(8000) is exceeded"
            self.invalidCode = 413 # https://stackoverflow.com/questions/686217/maximum-on-http-header-values
            return
        if i == -1 and len(request) < self.maxHeaderSize:
            self.invalid = True
            self.invalidReason = "2*CRLF not found"
            self.invalidCode = 400
            return
            
        # split request into "headers and body" part
        headers = request[:i].decode()
        body = request[i + 4:]
        
        splitHeader = headers.split("\r\n")
        requestLine = splitHeader[0]
        headers = splitHeader[1:]
        self._parseRequestLine(requestLine)
        self._parseHeaders(headers)

        self.body = body # message-body
        return
    
    def _validateRequest(self) -> None:
        """
        returns (invalid, errPkt)
        if invalid == True then send the errPkt
        """
        
        method = self.requestLine["method"]
        scheme = self.requestLine["URI"]["scheme"]
        host = self.requestLine["URI"]["host"]
        port = self.requestLine["URI"]["port"]
        protocol = self.requestLine["version"]["protocol"]
        majorVersion = self.requestLine["version"]["majorVersion"]
        minorVersion = self.requestLine["version"]["minorVersion"]
     
        # if there were syntactical errors while parsing

        if self.invalidCode != None: # if the invalid code has already been set then no need of this function
            return

        if self.invalid == True:
            self.invalidCode = 400
        
        # checking for semantic errors in request:
        elif method not in self.methods: # The method is case-sensitive.
            self.invalid = True
            self.invalidReason = method + " not supported for current URL"
            self.invalidCode = 501
            self.sendHeaders = { # Also send this "Allow" header along with the invalid status code
                "Allow": "GET HEAD POST PUT DELETE" # TODO - Do this dynamically if possible
            }
        elif scheme != "" and scheme != "http":
            self.invalid = True
            self.invalidCode = 400
            self.invalidReason = "Invalid scheme " + scheme + " in URI"

        elif host != "" and (host != "localhost" and host != "127.0.0.1"):
            self.invalid = True
            self.invalidCode = 400
            self.invalidReason = "Invalid host " + host + " in URI"
        elif port != self.port:
            self.invalid = True
            self.invalidCode = 400
            self.invalidReason = "Invalid port " + str(port) + " in URI"
        elif protocol != "HTTP":
            self.invalid = True
            self.invalidCode = 400
            self.invalidReason = "Invalid protocol " + protocol + " in Request Line"
        elif majorVersion != 1 or minorVersion != 1:
            self.invalid = True
            self.invalidCode = 505
            self.invalidReason = "Invalid Version Number. Accepted Version: HTTP/1.1"
            
        if not self.invalid and self.headers.get("host") == None:
            self.invalid = True
            self.invalidCode = 400
            self.invalidReason = "Host header field absent"
        elif not self.invalid and host == "": # if host in URI is empty then check
            hostHeader = self.headers["host"]
            hostnameHeader = hostHeader.split(":", 1)[0]
            try:
                portHeader = int(hostHeader.split(":", 1)[1])
            except:
                portHeader = self.port
            
            if hostnameHeader != "localhost" and hostnameHeader != "127.0.0.1":
                self.invalid = True
                self.invalidCode = 400
                self.invalidReason = "Invalid host " + hostnameHeader + " in Host header"
            elif portHeader != self.port:
                self.invalid = True
                self.invalidCode = 400
                self.invalidReason = "Invalid port " + str(portHeader) + " in Host Header"
        return
