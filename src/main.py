from server import HttpServer

def main() -> None:
    server = HttpServer()
    try:
        server.start()
    except Exception as e:
        f = open("../error.log", "a")
        f.write(str(e))
        f.close()
    return

if __name__ == "__main__":
    main()
