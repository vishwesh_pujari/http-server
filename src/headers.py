import datetime # for Date header
import pytz # for inter-conversion between time zones
import hashlib # for generating MD5 hash for Content-MD5
import base64 # for generating base64 encoded md5 hash
import os
import uuid

class Headers:
    def __init__(self, name: str, version: float, encodings: list) -> None:
        self.name = name
        self.version = version
        self.encodings = encodings
        #self.IP = IP
        #self.port = port
        return
        
    def parseDate(self, strDate: str) -> datetime.datetime:
    	# https://docs.python.org/3/library/datetime.html#strftime-and-strptime-behavior
        pattern1 = "%a, %d %b %Y %H:%M:%S %Z" # Sun, 06 Nov 1994 08:49:37 GMT
        pattern2 = "%A, %d-%b-%y %H:%M:%S %Z" # Sunday, 06-Nov-94 08:49:37 GMT
        pattern3 = "%a %b %d %H:%M:%S %Y" # Sun Nov 06 08:49:37 1994

        try:
            t = datetime.datetime.strptime(strDate, pattern1) 
        except:
            try:
                t = datetime.datetime.strptime(strDate, pattern2)
            except:
                try:
                    t = datetime.datetime.strptime(strDate, pattern3)
                except:
                    return datetime.datetime.strptime("", "") # returns the default value of datetime.datetime(1900, 1, 1, 0, 0)

        if pytz.UTC.localize(t) > datetime.datetime.now(pytz.timezone("UTC")): # A date which is later than the server's current time is invalid.
            return datetime.datetime.strptime("", "")
        return t

    def _qFactor(self, header: str) -> list:
        # this function handles all those headers, which use the "q" factor
        split = header.split(",")
        acceptable = []
        
        # separate the data and 'q' factor
        for i in range(len(split)):
            split[i] = split[i].strip()
            data = split[i].split(";")[0]
            try:
                pref = split[i].split(";")[1] # q value
                #assert split[i].split("=")[0] == "q"
                pref = pref.strip()
                assert pref.split("=")[0] == "q"
                pref = float(pref.split("=")[1])
            except:
                pref = 1
            if pref > 1 or pref <= 0: # invalid preference
                pass
            else:
                acceptable.append([data, pref])

        split = acceptable
        # sort according to q preferences (highest first)
        split = sorted(split, key = lambda ele: ele[1], reverse = True)
        # remove the q factor
        for i in range(len(split)):
            split[i] = split[i][0]
        return split

    def date(self) -> str:
        # returns date in format : Sun, 06 Nov 1994 08:49:37 GMT
        
        """
        For the purposes of HTTP, GMT is exactly
        equal to UTC (Coordinated Universal Time).
        """
        dObj = datetime.datetime.now(pytz.timezone("UTC"))
        return dObj.strftime("%a, %d %b %Y %H:%M:%S GMT")

    def server(self) -> str:
        return self.name + "/" + str(self.version)

    def accept(self, ah: str) -> list:
        # takes ah(accept header value) as input and returns list of mimetypes acc to preference
        # as output
        
        # TODO - write logic in accept, charset and encoding
        split = self._qFactor(ah)
        for i in range(len(split)):
            split[i] = split[i].split("/")
        
        return split
    
    def accept_charset(self, ach: str) -> list:
        # ach - accept charset header
        split = self._qFactor(ach)
        if "*" not in split:
            split.insert(0, "ISO-8859-1") # If no "*" is present in an Accept-Charset field, then all character sets not explicitly mentioned get a quality value of 0, except for ISO-8859-1, which gets a quality value of 1 if not explicitly mentioned.
        return split
    
    def accept_encoding(self, aeh: str) -> list:
        split = self._qFactor(aeh)
        return split
    
    def last_modified(self, date: datetime.datetime) -> str:
        return date.strftime("%a, %d %b %Y %H:%M:%S GMT")
    
    def if_modified_since(self, modifiedDate: datetime.datetime, strReqDate: str) -> bool:
        reqDate = self.parseDate(strReqDate)
        if reqDate == datetime.datetime.strptime("", ""): # if requested date is invalid
            return True # ignore this header(send normal response) if date is invalid
        if modifiedDate <= reqDate:
            return False # means that resource hasn't been modified
        return True # means that resource has been modified

    def if_unmodified_since(self, modifiedDate: datetime.datetime, strReqDate: str) -> bool:
        reqDate = self.parseDate(strReqDate)
        if reqDate == datetime.datetime.strptime("", ""): # if requested date is invalid
            return True # ignore this header(send normal response) if date is invalid
        if modifiedDate > reqDate:
            return False # means that resource has been modified
        return True # means that resource is unmodified

    def content_type(self, cth: str) -> list:
        # cth - content type header
        cth = cth.strip()
        type = cth.split(";", 1)[0].strip().split("/")
        try:
            field = cth.split(";", 1)[1].strip().split("=", 1)[0]
            value = cth.split(";", 1)[1].strip().split("=", 1)[1]
            if field == "charset":
                charset = value
            elif field == "boundary":
                boundary = value
        except:
            charset = ""
            boundary = ""
        if (type[0] == "application" and type[1] == "x-www-form-urlencoded") or (type[0] == "text"):
            type.append(charset)
        elif type[0] == "multipart" and type[1] == "form-data":
            type.append(boundary)
        return type

    def content_encoding(self, ceh: str) -> tuple:
        ceh = ceh.strip()
        if ceh not in self.encodings:
            return False, ceh
        return True, ceh
    
    def content_disposition(self, cdh: str) -> tuple:
        split = cdh.split(";")
        try:
            assert split[0].strip() == "form-data"
            assert len(split) >= 2
            assert split[1].strip().split("=", 1)[0].strip() == "name"
        except:
            return (False,)
        
        name = split[1].strip().split("=", 1)[1].strip() 
        if name[0] == '"' and name[-1] == '"':
            name = name[1:-1] # first and last are "
        filename = ""
    
        if len(split) == 3:
            try:
                assert split[2].strip().split("=", 1)[0].strip() == "filename"
            except:
                return (True,name)
            filename = split[2].strip().split("=", 1)[1].strip()[1:-1]
            if len(filename) > 0 and filename[0] == '"' and filename[-1] == '"':
                filename = filename[1:-1]
            return True, name, filename
        
        return True, name

    def range(self, rh: str, fileLength: int) -> list:
        rh = rh.strip()
        unit_range = rh.split("=", 1)
        unit = unit_range[0].strip()
        try:
            Range = unit_range[1].strip()
        except:
            # Range not specified
            raise Exception("Range not specified in Range Header")
        try:
            assert unit == "bytes"
        except:
            raise Exception("Unit must be bytes. The specified unit is " + unit)
        Range = Range.split(",")
        
        i = 0
        while i < len(Range):
            Range[i] = Range[i].split("-", 1)
            Range[i][0] = Range[i][0].strip()
            try:
                Range[i][1] = Range[i][1].strip()
            except:
                raise Exception("Range end not provided for " + str(Range[i][0]))
            
            if Range[i][0] == "" and Range[i][1] == "":
                raise Exception("Both Range start and end can't be empty")

            # if starting of range is provided
            if Range[i][0] != "":
                try:
                    Range[i][0] = int(Range[i][0])
                    if Range[i][0] < 0:
                        raise Exception("Range start negative")
                except:
                    raise Exception("Invalid Range Start " + str(Range[i][0]) + ". Expected integer")
            if Range[i][1] != "":
                try:
                    Range[i][1] = int(Range[i][1])
                    if Range[i][1] < 0:
                        raise Exception("Range end negative")
                except:
                    raise Exception("Invalid Range End " + str(Range[i][1]) + ". Expected integer")
            
            # if starting of range isn't provided
            if Range[i][0] == "":
                # the client is expecting last Range[i][1] bytes
                Range[i][0] = fileLength - Range[i][1]
                Range[i][1] = fileLength - 1
            if Range[i][1] == "":
                Range[i][1] = fileLength - 1

            if Range[i][1] < Range[i][0]:
                raise Exception( str(Range[i][1]) + " less than " + str(Range[i][0]))
            
            i += 1

        Range = sorted(Range, key = lambda ele: ele[0])

        i = 0
        while i < len(Range) - 1:
            currStart = Range[i][0]
            currEnd = Range[i][1]

            # for handling cases like [[3, 4], [3, 5]]. i.e same currStart
            inLoop = False
            while i < len(Range) - 1 and currStart == Range[i + 1][0]:
                if currEnd != Range[i + 1][1]:
                    raise Exception("Overlapping Ranges")
                i += 1
                inLoop = True

            if inLoop:
                i -= 1
            if i >= len(Range) - 1:
                break
            
            # [[1, 4], [2, 5], [3, 5], [3, 4], [4, 5]]
            if currEnd > Range[i + 1][0]:
                raise Exception("Overlapping Ranges")
            i += 1
            
        return Range
    
    def content_md5(self, data: bytes) -> str:
        # The MD5 message-digest algorithm is a widely used 
        # cryptographic hash function producing a 128-bit 
        # (16-byte) hash value.
        # Base64 encoding is used to represent binary data in an ASCII 
        # string - every char is 6 bits

        hash = base64.b64encode(hashlib.md5(data).digest()).decode()
        return hash

    def get_ETag(self, filename: str) -> str:
        # TODO  try to modify the ETag!
        return str(os.path.getmtime(filename))

    # returns True if entity has been modified, else returns false
    def if_range(self, ifh: str, filename: str, modifiedDate: datetime.datetime) -> bool:
        # The server can distinguish between a valid HTTP-date and 
        # any form of entity-tag by examining no more than two characters.

        # https://datatracker.ietf.org/doc/html/rfc2616#section-14.19
        if ifh[0] == '"' and ifh[-1] == '"':
            form = "ETag"
        elif ifh[:3] == 'W/"':
            form = "ETag"
        else:
            form = "Date"
        if form == "ETag":
            if '"' + self.get_ETag(filename) + '"' != ifh:
                return True # entity has been changed
            else:
                return False
        
        elif form == "Date":
            reqDate = self.parseDate(ifh)
            if reqDate == datetime.datetime.strptime("", ""): # if requested date is invalid
                return True # ignore this header(send normal response) if date is invalid
            if modifiedDate <= reqDate:
                return False # means that resource hasn't been modified
            return True # means that resource has been modified

    def set_cookie(self) -> str:
        cookieValue = str(uuid.uuid4())
        return cookieValue

    def cookie(self, ch: str) -> dict: # ch- cookie header
        ch = ch.strip().split(";")
        
        for i in range(len(ch)):
            ch[i] = ch[i].strip().split("=")
            ch[i][0] = ch[i][0].strip()
            try:
                ch[i][1] = ch[i][1].strip()
            except:
                raise Exception("Cookie value not present for " + ch[i][0])
        return ch            