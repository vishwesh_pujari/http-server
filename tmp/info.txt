1. You can check disable-cache in Browser Network Tab!
2. Try Transferring ubuntu-logo downloaded in tmp/photos bcoz of its less size
3. IMP - To construct HTTP queries - Go to Network Tab-Headers-"Resend" in front of Filter: Edit and Resend
4. Transferred315 B (2.90 GB size) means that Response Headers size is 315Bytes and data size is 2.90GB
5. Download Large File: https://cabulous.medium.com/how-http-delivers-a-large-file-78af8840aad5
                        https://developer.mozilla.org/en-US/docs/Web/HTTP/Range_requests
6. To make request to apache, open socket and send req to "127.0.0.1" on port 80
7. In HTTP/1.1 persistent connections are the default, but sir has asked us to do the opposite
8. In a POST request, the form data is actually sent to a server, and the server sends it to the php or python interpreter. But we aren't going to do that. We will try to do a work-around for this. When some data is sent as a POST data, then it is expected to go as a "subordinate" of the URI. So if we are sending POST request to /something, then the data should be stored as something.post1 (or post1.something) indicating that post1 is subordinate of something!
9. PUT will create that particular file and then store the data in that file. It is not concerned with "subordinate" part
10. Max headers size: https://stackoverflow.com/questions/686217/maximum-on-http-header-values
			https://www.geekersdigest.com/max-http-request-header-size-server-comparison/

11. TCP uses a listen socket, and if someone connects then a different socket for communication
    with that client is used. So I used to think that this socket would have different port number(other than 80) 
    than the listen socket. But this is not true! The listen socket and the communication sockets all 
    have same port number. This is
    because Connections are uniquely identified by the OS by the following 5-tuple: 
    (local-IP, local-port, remote-IP, remote-port, protocol). 
    If any element in the tuple is different, then this is a completely independent connection. And either of client
    ip or port is going to be different in a connection

    So even if on http server side different sockets have been opened, all communicate via the port 80!
    
12. https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types - Common mime types

https://stackoverflow.com/questions/37484888/enable-put-and-delete-methods-on-apache-2-4

13. LogLevel only for error log : https://httpd.apache.org/docs/2.4/mod/core.html#loglevel
https://stackify.com/apache-error-log-explained/
https://github.com/AnupNair08/mHTTP-Server
https://docs.python.org/3/howto/logging.html
https://docs.python.org/3/library/logging.html#logrecord-attributes

Apache2 - Access log format : https://www.sumologic.com/blog/apache-access-log/