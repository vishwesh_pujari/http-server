Job of a server

1. Receive a request
2. Parses the request
3. Pass on the request to the appropriate handler - GET|POST|PUT
4. Get a valid response from the handler
5. If request asks for keeping connection alive then do multi-threading
6. Send the response back to the client

Headers:

https://datatracker.ietf.org/doc/html/rfc2616#section-4.5
general-header = Cache-Control            ; Section 14.9
                      | Connection               ; Section 14.10
                      | Date                     ; Section 14.18
                      | Pragma                   ; Section 14.32
                      | Trailer                  ; Section 14.40
                      | Transfer-Encoding        ; Section 14.41
                      | Upgrade                  ; Section 14.42
                      | Via                      ; Section 14.45
                      | Warning                  ; Section 14.46
                      

https://datatracker.ietf.org/doc/html/rfc2616#section-5.3                   
request-header = Accept                   ; Section 14.1
                      | Accept-Charset           ; Section 14.2
                      | Accept-Encoding          ; Section 14.3
                      | Accept-Language          ; Section 14.4
                      | Authorization            ; Section 14.8
                      | Expect                   ; Section 14.20
                      | From                     ; Section 14.22
                      | Host                     ; Section 14.23
                      | If-Match                 ; Section 14.24
                      | If-Modified-Since        ; Section 14.25
                      | If-None-Match            ; Section 14.26
                      | If-Range                 ; Section 14.27
                      | If-Unmodified-Since      ; Section 14.28
                      | Max-Forwards             ; Section 14.31
                      | Proxy-Authorization      ; Section 14.34
                      | Range                    ; Section 14.35
                      | Referer                  ; Section 14.36
                      | TE                       ; Section 14.39
                      | User-Agent               ; Section 14.43
                      
https://datatracker.ietf.org/doc/html/rfc2616#section-6.2  
response-header =        Accept-Ranges           ; Section 14.5
                       | Age                     ; Section 14.6
                       | ETag                    ; Section 14.19
                       | Location                ; Section 14.30
                       | Proxy-Authenticate      ; Section 14.33
                       | Retry-After             ; Section 14.37
                       | Server                  ; Section 14.38
                       | Vary                    ; Section 14.44
                       | WWW-Authenticate        ; Section 14.47
                       
                       
https://datatracker.ietf.org/doc/html/rfc2616#section-7.1
entity-header  =        Allow                    ; Section 14.7
                      | Content-Encoding         ; Section 14.11
                      | Content-Language         ; Section 14.12
                      | Content-Length           ; Section 14.13
                      | Content-Location         ; Section 14.14
                      | Content-MD5              ; Section 14.15
                      | Content-Range            ; Section 14.16
                      | Content-Type             ; Section 14.17
                      | Expires                  ; Section 14.21
                      | Last-Modified            ; Section 14.29
                      | extension-header



Headers to be implemented:

https://datatracker.ietf.org/doc/html/rfc2616#section-4.5
general-header = 
                      | Connection               ; Section 14.10
                      | Date                     ; Section 14.18
                      | Transfer-Encoding        ; Section 14.41
                      

https://datatracker.ietf.org/doc/html/rfc2616#section-5.3                   
request-header =        Accept                   ; Section 14.1
                      | Accept-Charset           ; Section 14.2
                      | Accept-Encoding          ; Section 14.3
                      | Host                     ; Section 14.23
                      | If-Modified-Since        ; Section 14.25
                      | If-Unmodified-Since      ; Section 14.28
                      | If-Range                 ; Section 14.27
                      | Range                    ; Section 14.35
                      | User-Agent               ; Section 14.43 # only for statistical purposes - Just log it!
                      | TE
                      
https://datatracker.ietf.org/doc/html/rfc2616#section-6.2  
response-header =        Accept-Ranges           ; Section 14.5
                       | ETag                    ; Section 14.19 # https://stackoverflow.com/questions/4533/http-generating-etag-header, https://www.geeksforgeeks.org/generating-random-ids-using-uuid-python/
                       | Location                ; Section 14.30 # USE location when /tmp is given and tmp is a directory: /tmp/
                       | Server                  ; Section 14.38
                       
                       
https://datatracker.ietf.org/doc/html/rfc2616#section-7.1
entity-header  =        Allow                    ; Section 14.7
                      | Content-Encoding         ; Section 14.11
                      | Content-Length           ; Section 14.13
                      | Content-Location         ; Section 14.14
                      | Content-MD5              ; Section 14.15 # https://www.geeksforgeeks.org/md5-hash-python/
                      | Content-Type             ; Section 14.17
                      | Expires                  ; Section 14.21
                      | Last-Modified            ; Section 14.29

Headers to be implemented:

Accept
Accept-Charset
Content-Encoding
Accept-Encoding
Content-Length
Content-MD5
Content-Type
Content-Type
Date
Host
If-Modified-Since
If-Range
If-Unmodified-Since
Range
User-Agent
Accept-Ranges
Content-Location
ETag
Expires
Last-Modified
Location
Server
Set-Cookie # THIS HEADER NOT IN RFC
Transfer-Encoding
Connection
Keep-Alive # THIS HEADER NOT IN RFC
Allow

--
And all the other headers that need to be implemented to ensure that the above headers can be implemented.
