# http-server

http-server is a HTTP/1.1 compliant web server based on RFC2616. Handles GET,HEAD,POST,PUT,DELETE methods. Supports multi-threading, persistent and non-persistent connections, cookies, most status codes and headers.
