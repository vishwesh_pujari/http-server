#!/usr/bin/python3

#TODO - suggested by abhijit sir : retry dns query if rcode is not good.

from sys import argv
from threading import Thread
from socket import socket, AF_INET, SOCK_DGRAM, SOCK_STREAM

# myThread is a child-class of Thread class
class myThread(Thread):
    # override the init method of Thread class
    def __init__(self, function, hostname, pathname, filename):
        Thread.__init__(self)
        self.function = function # function is to be called later
        self.hostname = hostname
        self.pathname = pathname
        self.filename = filename
    
    # override the run() method 
    def run(self): 
        self.function(self.hostname, self.pathname, self.filename) # call the function

"""
getIP()
Input       : hostname
Return value: (RCODE, IP_Address)
Performs dns query
"""
def getIP(hostname):
    # the primary dns server for Google DNS
    dnsServer = "8.8.8.8"
    dnsServerPort = 53 
    
    # DNS Queries won't fail if we set RecursionDesired = 1, bcoz then the load is not on 8.8.8.8 
    # queries sent to 8.8.8.8 may fail if load is a lot
    # HEADER
    headerBytes = bytes([0x12, 0x12, # ID
            0x01, 0x00, # Flags - setting RD=1(Recursion Desired=1)
            0x00, 0x01, # QDCOUNT = QuestionCount = 1
            0x00, 0x00, # ANCOUNT
            0x00, 0x00, # NSCOUNT
            0x00, 0x00 # ARCOUNT
            ])

    # QUESTION
    # QNAME

    questionBytes = b""
    breakHostname = hostname.split(".")
    for part in breakHostname:
        questionBytes += bytes([len(part)]) + bytes(part, "utf-8")
    # domain name terminates with 0 length octet
    questionBytes += bytes([0x00])

    questionBytes += bytes([0x00, 0x01, # QTYPE - 1 Type A
                            0x00, 0x01]) # QCLASS - 1 IN

    query = headerBytes + questionBytes

    # create UDP client socket
    clientSocket = socket(AF_INET, SOCK_DGRAM)
    clientSocket.sendto(query, (dnsServer, dnsServerPort))
    reply, addr = clientSocket.recvfrom(4096)

    # Lower nibble of 4th byte in 'reply' is RCODE - Response Code
    # RCODE=0 indicates success
    RCODE = reply[3] & 0x0F # BITWISE AND with 0x0F so as to get lower nibble
    
    #Last 4 bytes = IP address of host if RCODE=0
    IP_Address = ""
    for byte in reply[-4:]:
        IP_Address += str(byte) + "."
    # remove the last decimal point
    IP_Address = IP_Address[:-1]
    return (RCODE, IP_Address)

"""
breakUrls()
Input : list of urls
Output: (hostnames,pathnames,filenames) all lists corresponding to urls list
"""
def breakUrls(urls):
    hostnames = []
    pathnames = []
    filenames = []
    
    # to keep a count of how many times a filename occurs
    fileNameCount = dict()

    for url in urls:
        # split on the first occurence of "/"
        splitUrl = url.split("/", 1)
        hostnames.append(splitUrl[0])
        if len(splitUrl) == 1:
            pathnames.append("")
            currFilename = "index.html"
        else:
            pathnames.append(splitUrl[1])
            currFilename = splitUrl[1].split("/")[-1]
	
	# if currFilename not yet in the dictionary
        if fileNameCount.get(currFilename) == None:
            filenames.append(currFilename)
            fileNameCount[currFilename] = 1
        # if currFilename is in dictionary, then there is a clash in names
        else:
            filenames.append(currFilename.split(".")[0] + str(fileNameCount[currFilename]) + "." +currFilename.split(".")[1])
            fileNameCount[currFilename] += 1

    return hostnames, pathnames, filenames

def httpQuery(hostname, path, filename):
    # call this repeated times on separate threads
    
    print("fetching " + hostname + "/" + path)
    
    RCODE, IP = getIP(hostname)
    if (RCODE == 2):
        print(hostname + ": Server Failure - Name server was unable to process this query due to a problem with name server")
        return
    if (RCODE == 3):
        print(hostname + ": Name error - Host doesn't exist")
        return
    
    # create TCP client socket
    s = socket(AF_INET, SOCK_STREAM)
    s.connect((IP, 80))
    message = "GET /" + path + " HTTP/1.1\r\n"
    message += "HOST: " + hostname + "\r\n"
    message += "\r\n"
    x = s.send(message.encode())
    
    # set timeout in seconds for recv()
    s.settimeout(3)
    data = s.recv(1024).decode()
    statusString = data.split("\r\n")[0].split(" ", 1)[1]
    
    statusCode = int(statusString.split(" ")[0])

    if statusCode != 200:
        print(hostname + path + " : " + statusString)
        return

    f = open(filename, "w")

    # extract the html part of response
    data = data.split("\r\n\r\n")[1]
    while (len(data)):
        f.write(data)
        try:
            data = s.recv(1024).decode()
        except: # when timeout occurs
            break
    f.close()
    print("saved " + hostname + "/" + path)
    return

def main():
    if len(argv) == 1:
        print("usage: python3 dns-client.py <hostname>")
        exit()
    
    hostNames, pathNames, filenames = breakUrls(argv[1:])

    for i in range(len(hostNames)):
    	myThread(httpQuery, hostNames[i], pathNames[i], filenames[i]).start()

    return

if (__name__ == "__main__"):
    main()
