import socket
import gzip
import webbrowser
import os

serverName = "127.0.0.1"
serverPort = 8080

clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
clientSocket.connect((serverName, serverPort))

InvalidRequestsParsing = [

# 1. Invalid Request Line
b"Hello World\r\nHost: localhost\r\nConnection: keep-alive\r\n\r\n",

# 2. Invalid Path in Request Line
b"GET mypath HTTP/1.1\r\nHost: localhost\r\nConnection: keep-alive\r\n\r\n",

# 3. Version number not specified
b"GET / HTTP\r\nHost: localhost\r\nConnection: keep-alive\r\n\r\n",

# 4. Invalid Version Number
b"GET / HTTP/string.string\r\nHost: localhost\r\nConnection: keep-alive\r\n\r\n",

# 5. Field Value not present for a header
b"GET / HTTP/1.1\r\nHost: localhost\r\nConnection\r\n\r\n",

# 6. Max header size exceeded
b"GET / HTTP/1.1\r\nHost: localhost" + b"a" * 8000,

# 7. 2*CRLF not found
b"GET / HTTP/1.1\r\nHost: localhost",

# 8. Invalid Method
b"MYMETHOD / HTTP/1.1\r\nHost: localhost\r\nConnection: keep-alive\r\n\r\n",

# 9. Invalid Scheme(https) in URL
b"GET https://localhost:8080/ HTTP/1.1\r\nHost: localhost\r\nConnection: keep-alive\r\n\r\n",

# 10. Invalid host in URL
b"GET http://myhost:8080/ HTTP/1.1\r\nHost: localhost\r\nConnection: keep-alive\r\n\r\n",

# 11. Invalid port in URL
b"GET http://127.0.0.1:80/ HTTP/1.1\r\nHost: localhost\r\nConnection: keep-alive\r\n\r\n",

# 12. Invalid Host HTTPS in URL
b"GET / HTTPS/1.1\r\nHost: localhost\r\nConnection: keep-alive\r\n\r\n",

# 13. Invalid Version Number
b"GET / HTTP/11.11\r\nHost: localhost\r\nConnection: keep-alive\r\n\r\n",

# 14. Host header field isn't present
b"GET / HTTP/1.1\r\nConnection: keep-alive\r\n\r\n",

# 15. Invalid Host header
b"GET / HTTP/1.1\r\nHost: myhost\r\nConnection: keep-alive\r\n\r\n"
]

InvalidRequestsGetHead = [

# 1. File not present - 404 Not Found
b"GET /myfile HTTP/1.1\r\nHost: localhost\r\nConnection: keep-alive\r\n\r\n",

# 2. Read permission not on the file "hello" - 403 Forbidden
b"GET /hello HTTP/1.1\r\nHost: localhost\r\nConnection: keep-alive\r\n\r\n",

# 3. Requesting for an image but specifying only Accept: text/plain - 406 Not Acceptable
b"GET /me.jpg HTTP/1.1\r\nHost: localhost\r\nAccept: text/plain\r\nConnection: keep-alive\r\n\r\n",

# 4. Requesting for Charset utf-8 for image file - 406 Not Acceptable
b"GET /me.jpg HTTP/1.1\r\nHost: localhost\r\nAccept: image/*\r\nAccept-Charset: utf-8\r\nConnection: keep-alive\r\n\r\n",

# 5. Requesting for Invalid Encoding - 406 Not Acceptable
b"GET /me.jpg HTTP/1.1\r\nHost: localhost\r\nAccept: image/*\r\nAccept-Charset: *\r\nAccept-Encoding: myencoding\r\nConnection: keep-alive\r\n\r\n",

# 6. Requesting file which is unmodified since date - 304 Not Modified
b"GET / HTTP/1.1\r\nHost: localhost\r\nAccept: text/*\r\nAccept-Charset: *\r\nAccept-Encoding: identity\r\nConnection: keep-alive\r\nIf-Modified-Since: Sat, 13 Nov 2021 06:27:21 GMT\r\n\r\n",

# 7. Requesting file which has been modified since date - 412 PreCondition failed
b"GET / HTTP/1.1\r\nHost: localhost\r\nAccept: text/*\r\nAccept-Charset: *\r\nAccept-Encoding: identity\r\nConnection: keep-alive\r\nIf-UnModified-Since: Sat, 12 Nov 2021 06:27:21 GMT\r\n\r\n", # bcoz file has been modified since 12 Nov

# 8. Requesting for Transfer encoding which isn't implemented - 501 Not implemented
b"GET / HTTP/1.1\r\nHost: localhost\r\nAccept: text/*\r\nAccept-Charset: *\r\nAccept-Encoding: identity\r\nConnection: keep-alive\r\nTE: myte\r\n\r\n",

# 9. Giving Overlapping Ranges
b"GET / HTTP/1.1\r\nHost: localhost\r\nAccept: text/*\r\nAccept-Charset: *\r\nAccept-Encoding: identity\r\nConnection: keep-alive\r\nRange: bytes=1-10,6-9\r\n\r\n"
]

f = open("non-ascii", "r")
nonAsciiData = f.read()
f.close()
InvalidRequestsPutPost = [
# 1. Invalid Content-Length header - 400 Bad Request
b"PUT /newfile HTTP/1.1\r\nHost: localhost\r\nContent-Length: string\r\nContent-Type: text/plain\r\nConnection: keep-alive\r\n\r\n",

# 2. Content-Length header absent - 411 Length Required
b"PUT /newfile HTTP/1.1\r\nHost: localhost\r\nConnection: keep-alive\r\n\r\n",

# 3. Content-Encoding not supported
b"PUT /newfile HTTP/1.1\r\nHost: localhost\r\nContent-Encoding: mytype\r\nContent-Length: 5\r\nConnection: keep-alive\r\n\r\nHello",

# 4. Setting Content-Encoding value to deflate, but encoding data to gzip!
b"PUT /newfile HTTP/1.1\r\nHost: localhost\r\nContent-Encoding: deflate\r\nContent-Length: " + str(len(gzip.compress(b"Hello"))).encode() + b"\r\nConnection: keep-alive\r\n\r\n" + gzip.compress(b"Hello"),

# 5. Sending us-ascii as the charset, whereas the content is actually utf-8
b"PUT /newfile HTTP/1.1\r\nHost: localhost\r\nContent-Type: text/plain; charset=us-ascii\r\nContent-Length: 1\r\nConnection: keep-alive\r\n\r\n" + nonAsciiData.encode("utf-8")
]

InvalidRequestsPut = [
# 1. Sending PUT request on a directory - 405 Method Not allowed
b"PUT / HTTP/1.1\r\nHost: localhost\r\nContent-Type: text/plain; charset=us-ascii\r\nContent-Length: 5\r\nConnection: keep-alive\r\n\r\nHello"
]

InvalidRequestsDelete = [
# 1. Sending Delete request on a non-existing path
b"DELETE /non-existing HTTP/1.1\r\nHost: localhost\r\n\r\n",

# 2. Sending delete on a non-empty directory - 403 Forbidden
b"DELETE /pdfs HTTP/1.1\r\nHost: localhost\r\n\r\n"
]

GET = [
"http://localhost:8080", # this also tests for gzip encoding because browser requests in gzip format
"http://localhost:8080/me.jpg",
"http://localhost:8080/mypdf.pdf"
]

HEAD = [
b"HEAD http://localhost:8080 HTTP/1.1\r\nHost: localhost\r\nConnection: keep-alive\r\n\r\n",
b"HEAD http://localhost:8080/me.jpg HTTP/1.1\r\nHost: localhost\r\nConnection: keep-alive\r\n\r\n",
b"HEAD http://localhost:8080/mypdf.pdf HTTP/1.1\r\nHost: localhost\r\nConnection: keep-alive\r\n\r\n"
]

POST = [
"./test_posturlencoded.html",
"./test_multipartformdata.html"
]

acadCalendar = open("./Academic Calendar - TY - Final UG.pdf", "rb")
data1 = acadCalendar.read()
acadCalendar.close()
textFile = open("./mywget.py", "r")
data2 = textFile.read()
textFile.close()
PUT = [
b"PUT /pdfs/AcadCalendar.pdf HTTP/1.1\r\nHost: localhost\r\nContent-Type: application/pdf\r\nContent-Length: " + str(len(data1)).encode() + b"\r\n\r\n" + data1,
b"PUT /mywget.py HTTP/1.1\r\nHost: localhost\r\nContent-Type: text/plain\r\nContent-Length: " + str(len(data2)).encode() + b"\r\n\r\n" + data2.encode()
]

DELETE = [
b"DELETE /mywget.py HTTP/1.1\r\nHost: localhost\r\n\r\n"
]

def makeRequest(req: bytes, clientSocket: socket.socket) -> socket.socket:
    if len(req) >= 8000:
        print("A very large request!\n\n")
    else:
        try:
            print(req.decode())
        except:
            print(req.split(b"\r\n\r\n", 1)[0].decode() + "\r\n\r\nBinary Data!")

    try:
        clientSocket.sendall(req)
    except:
        clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        clientSocket.connect((serverName, serverPort))
    response = clientSocket.recv(8000)
    print(response.decode())
    print("-" * 40)

    if b"Connection: close" in response:
        clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        clientSocket.connect((serverName, serverPort))

    if response.find(b"HTTP/1.1 408") != -1: # if timeout has happ again then send request again
        return makeRequest(req, clientSocket)
    return clientSocket

def makeRequests(requests: list, clientSocket: socket.socket) -> socket.socket:
    for req in requests:
        clientSocket = makeRequest(req, clientSocket)
    return clientSocket

print("Sending " + str(len(InvalidRequestsParsing)) + " Invalid formed Requests")
clientSocket = makeRequests(InvalidRequestsParsing, clientSocket)
input("Enter any key to continue")
os.system("clear")

print("Sending " + str(len(InvalidRequestsGetHead)) + " Invalid Requests for GET and HEAD Methods")
clientSocket = makeRequests(InvalidRequestsGetHead, clientSocket)
input("Enter any key to continue")
os.system("clear")

print("Sending " + str(len(InvalidRequestsPutPost)) + " Invalid Requests for PUT and POST Methods")
clientSocket = makeRequests(InvalidRequestsPutPost, clientSocket)
input("Enter any key to continue")
os.system("clear")

print("Sending " + str(len(InvalidRequestsPut)) + " Invalid Requests for PUT Method")
clientSocket = makeRequests(InvalidRequestsPut, clientSocket)
input("Enter any key to continue")
os.system("clear")

print("Sending " + str(len(InvalidRequestsDelete)) + " Invalid Requests for DELETE Method")
clientSocket = makeRequests(InvalidRequestsDelete, clientSocket)
input("Enter any key to continue")
os.system("clear")

print("Sending requests for GET through Browser!")
for req in GET:
    webbrowser.open(req)

input("Enter any key to continue")
os.system("clear")

print("Sending Requests for HEAD method")
clientSocket = makeRequests(HEAD, clientSocket)
input("Enter any key to continue")
os.system("clear")

print("Making Requests for POST throught browser!")
for req in POST:
    webbrowser.open(req)
input("Enter any key to continue")
os.system("clear")

print("Sending PUT requests")
for req in PUT:
    print(req.split(b"\r\n\r\n", 1)[0].decode() + "\r\n\r\nData!")
    try:
        clientSocket.sendall(req)
    except:
        clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        clientSocket.connect((serverName, serverPort))
    response = clientSocket.recv(8000)
    print(response.decode())
    print("-" * 40)

    if b"Connection: close" in response:
        clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        clientSocket.connect((serverName, serverPort))
    
    if response.find(b"HTTP/1.1 408") != -1: # if timeout has happ again then send request again
        print(req.split(b"\r\n\r\n", 1)[0].decode() + "\r\n\r\nData!")
        clientSocket.sendall(req)
        response = clientSocket.recv(8000)
        print(response.decode())
        print("-" * 40)

        if b"Connection: close" in response:
            clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            clientSocket.connect((serverName, serverPort))

input("Enter any key to continue")
os.system("clear")

print("Sending DELETE requests")
clientSocket = makeRequests(DELETE, clientSocket)
input("Enter any key to continue")
os.system("clear")

# for multi-threading testing - see the access.log file.

# testing for cookies
clientSocket.send(HEAD[0])
response = clientSocket.recv(8000)
i = response.find(b"Set-Cookie:")
i += len("Set-Cookie:")
response = response[i:]
cookie = response.split(b"\r\n", 1)[0].decode().strip()

for i in range(5):
    try:
        clientSocket.send(b"HEAD / HTTP/1.1\r\nHost: localhost\r\nCookie: " + cookie.encode() + b"\r\n\r\n")
    except:
        clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        clientSocket.connect((serverName, serverPort))
        clientSocket.send(b"HEAD / HTTP/1.1\r\nHost: localhost\r\nCookie: " + cookie.encode() + b"\r\n\r\n")
    try:
        response = clientSocket.recv(8000)
    except:
        clientSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        clientSocket.connect((serverName, serverPort))
        response = clientSocket.recv(8000)
print("check number of requests in cookie.json for " + cookie.split("=", 1)[1])